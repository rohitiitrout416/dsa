#include <iostream>
using namespace std;
#define N 10
class circular_queue
{
private:
    int front, rear;
    int arr[];

public:
    circular_queue()
    {
        front = -1;
        rear = -1;
        int *arr = new int[N];
    }

    void enqueue(int x)
    {
        if (front == ((rear + 1) % N))
            cout << "Sorry overflow";

        else if (front == -1)
        {
            front = rear = 0;
            arr[rear] = x;
        }

        else
        {
            rear = (rear + 1) % N;
            arr[rear] = x;
        }
    }

    void dequeue()
    {
        if (front == -1)
        {
            cout << "Sorry underflow";
        }

        else if (front == ((rear + 1) % N))
        {
            front = (rear + 2) % N;
        }

        else
        {
            front = ((front + 1) % N);
        }
    }

    void peek()
    {
        if (front == -1)
        {
            cout << "Queue is empty";
        }
        else
        {
            cout << arr[front] << endl;
        }
    }

    void display()
    {
        int i = 0;
        if (front == -1)
        {
            cout << "Queue is empty";
            return;
        }

        while (i != rear)
        {
            cout << "Element at " << i << "th position is " << arr[i] << endl;
            ;
            i = (i + 1) % N;
        }
    }
};

int main()
{
    circular_queue q;
    q.enqueue(5);
    q.enqueue(4);
    q.enqueue(3);
    q.enqueue(2);
    q.enqueue(1);
    q.display();
    q.peek();
    q.dequeue();
    q.dequeue();
    q.display();
    q.peek();
}
