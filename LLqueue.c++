//creating a queue using linked list
#include <iostream>
using namespace std;
//hello
//creating a node for linked list containing data and next
class node
{
public:
    int data;
    node *next;

    node(int val)
    {
        data = val;
        next = NULL;
    }
};

// creating a class named 'queue'
class queue
{
    node *front;
    node *back;

public:
    queue()
    {
        front = NULL;
        back = NULL;
    }
    
    void enqueue(int x)
    {
        node *n = new node(x);
        if (front == NULL)
        {
            back = n;
            front = n;
            return;
        }
        back->next = n;
        back = n;
    }

    void dequeue()
    {
        if (front == NULL)
        {
            cout << "Queue underflow" << endl;
            return;
        }
        node *todelete = front;
        front = front->next;
        delete todelete;
    }

    int peek()
    {
        if (front == NULL)
        {
            cout << "No element in queue" << endl;
            return -1;
        }
        cout << front->data << endl;
        return 0;
    }

    bool empty()
    {
        if (front == NULL)
            return true;
        return false;
    }
};

int main()
{
    queue q;
    q.enqueue(15);
    q.enqueue(05);
    q.enqueue(2002);
    q.peek();
    q.dequeue();
    q.peek();
    q.dequeue();
    q.peek();
    return 0;
}
