#include <iostream>
using namespace std;

class Node
{
public:
    int data;
    Node *next;

    Node(int val)
    {
        data = val;
        next = NULL;
    }
};

class stack
{
    Node *top;
public:
    stack()
    {
        top = NULL;
    }

    void push(int x)
    {
        Node *n = new Node(x);
        if (top == NULL)
        {
            top = n;
            return;
        }
        n->next = top;
        top = n;
    }

    void pop()
    {
        if(top == NULL)
        {
            cout<<"No element in stack";
            return;
        }
        Node* todelete = top;
        top = top->next;
        delete todelete;
    }

    bool isEmpty()
    {
        return top == NULL;
    }

    int peek()
    {
        if(top == NULL)
        {
            cout << "No element in stack";
            return -1;
        }
        cout<< top->data<<endl;
        return 0;
    }

    void display()
    {
        Node* n;
        if(top == NULL)
        {
            cout<<"No element in queue";
            return;
        }
        n = top;
        while (n != NULL)
        {
            cout<<n->data<<endl;
            n = n->next;
        }
    }
};

int main()
{
    stack st;
    st.push(15);
    st.push(05);
    st.push(2002);
    st.display();
    st.pop();
    st.pop();
    st.pop();
    st.peek();

    return 0;
}
