//Queue
#include <iostream>
using namespace std;
#define n 10

class Queue
{
    int front;
    int rear;
    int *arr;

public:
    Queue()
    {
        front = -1;
        rear = -1;
        arr = new int[n];
    }
    void enqueue(int x)
    {
        if (rear == n - 1)
        {
            cout << "queue is full/overflow" << endl;
            return;
        }
        front = 0;
        rear += 1;
        arr[rear] = x;
    }

    void dequeue()
    {
        if (front == -1)
        {
            cout << "queue is empty/underflow" << endl;
            return;
        }
        if (rear == 0)
        {
            front = -1;
        }
        front += 1;
    }
    void empty()
    {
        front = -1;
        rear = -1;
    }

    bool isEmpty()
    {
        return front == -1;
    }
    void Front()
    {
        cout << "Front element:" << arr[front] << " front = " << front << endl;
        ;
    }
    void Rear()
    {
        cout << "Rear element:" << arr[rear] << " rear = " << rear << endl;
    }
};

int main()
{
    Queue q;
    q.enqueue(1);
    q.enqueue(2);
    q.enqueue(3);
    q.Front();
    q.Rear();
    cout << q.isEmpty() << endl;
    q.dequeue();
    q.Front();
    q.Rear();
    q.empty();
    cout << q.isEmpty() << endl;
}